<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit387fb024661edfbced43616c48db01c5
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Akwad\\Guardian\\' => 27,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Akwad\\Guardian\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit387fb024661edfbced43616c48db01c5::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit387fb024661edfbced43616c48db01c5::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit387fb024661edfbced43616c48db01c5::$classMap;

        }, null, ClassLoader::class);
    }
}
