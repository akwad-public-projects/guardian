
<?php

return [
    "title" => [
        "default" => "حدث خطأ ما",
        "forbidden" => "بيانات غير صحيحة",
        "profile_not_found" => "لم يتم العثور على حسابك",
        "invalid_credentials" => "بيانات غير صحيحة",
        "technical_exception" => ".يرجى التواصل مع الدعم الفني",
        "invalid_otp" => "بيانات غير صحيحة",
    ],
    
    "message" => [
        "default" => "حدث خطأ ما",
        "token_is_missing" => "Token is missing!",
        "token_blacklisted" => "Token is blackListed!",
        "token_invalid" => "Token is invalid!",
        "phone_is_already_taken" => "يوجد حساب لهذا الرقم",
        "profile_already_exists" => "الحساب مستخدم بالفعل",
        "not_verified" => "لم يتم تفعيل الحساب",
        "invalid_otp" => "رمز التحقق الذي ادخلته غير صحيح",
        'invalid_credentials' => 'بيانات غير صحيحة',
        "profile_not_found" => "للمزيد من المعلومات يرجى التواصل مع فريق العمليات في تجدد",
        "forbidden" => "حسابك غير مصرح له الدخول لهذا التطبيق",
        "technical_exception" => "رقم الخطأ: :error",
    ]
];
