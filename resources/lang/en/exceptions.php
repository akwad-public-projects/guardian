<?php

return [
    "title" => [
        "default" => "Something went wrong",
        "forbidden" => "Something went wrong",
        "profile_not_found" => "We couldn't find your account",
        'invalid_credentials' => 'Invalid information',
        "technical_exception" => "Please contact technial support.",
        "invalid_otp" => "Something went wrong",
    ],
    
    "message" => [
        "default" => "Something went wrong",
        "token_is_missing" => "Token is missing!",
        "token_blacklisted" => "Token is blackListed!",
        "token_invalid" => "Token is invalid!",
        "phone_is_already_taken" => "Phone number is already taken by another account",
        "profile_already_exists" => "Account exists already",
        "not_verified" => "Account is not verified yet",
        "invalid_otp" => "The OTP you entered is incorrect",
        'invalid_credentials' => 'The password you entered is incorrect',
        "profile_not_found" => "Please contact us for more information",
        "forbidden" => "Your account is not allowed to access this app",
        "technical_exception" => "Error Number: :error.",
    ]
];
