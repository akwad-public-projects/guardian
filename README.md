<div align="center">
  <a href="https://akwadtech.com/">
    <img src="./resources/images/logo.gif" alt="guardian-logo" width="264" height="300">
  </a>
</div>

<div align="center">

# Guardian

**Guardian is an authentication assistant for Laravel**

</div>

## Author:

<div align="center">
  <a href="https://akwadtech.com/">
    Akwad Tech
  </a>
</div>
