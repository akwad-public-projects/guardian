<?php

return [
    "JWT_SECRET" => env("JWT_SECRET"),
    'SHOULD_VERIFY_KEYS' => explode(',', str_replace(" ","",env('SHOULD_VERIFY_KEYS',""))),
    'AUTHENTICATION_PRIORITY' => explode(',', str_replace(" ","",env('AUTHENTICATION_PRIORITY',"jwt_token,token,email,phone"))),
];
