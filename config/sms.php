<?php

return [
    'test_numbers' => explode(',', env('TEST_NUMBERS',"")),
    "expire_in_minutes" => env("SMS_EXPIRE_IN_MINUTES", 5),
    "test_mode" => env("SMS_OTP_TEST_MODE", false),
];