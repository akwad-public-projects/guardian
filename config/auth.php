<?php

return [

    'guards' => [
        'AuthUserGuard' => [
            'driver' => 'user_guard',
            'provider' => 'UserProvider',
            'hash' => false,
            'model' => null,
        ],
    ],

    'providers' => [
        'UserProvider' => [
            'driver' => 'user_provider',
        ],
    ],
];
