<?php

return [
    "400" => "default",
    "413" => "token_blacklisted",
    "414" => "token_invalid",
    "418" => "invalid_otp",
    "421" => "profile_not_found",
    "422" => "phone_is_already_taken",
    "427" => "forbidden",
    "433" => 'invalid_credentials',
    "450" => "profile_already_exists",
    "451" => "not_verified",
    "452" => "token_is_missing",
    "2000" => 'technical_exception',
    "2100" => 'technical_exception',
    "900" => 'technical_exception',
];
