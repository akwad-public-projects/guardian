<?php

return [
    'channels' => [
        'authentication' => [
            'driver' => 'daily',
            'path' => storage_path('logs/authentication/authentication.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'guards' => [
            'driver' => 'daily',
            'path' => storage_path('logs/guards/guards.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],
    ],
];
