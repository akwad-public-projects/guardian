<?php

namespace Akwad\Guardian\Providers;

use Akwad\Guardian\Guards\UserGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Akwad\Guardian\UserProviders\DefaultUserProvider;

class GuardianAuthenticationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Auth::provider('user_provider', function($app, array $config) {
            return new DefaultUserProvider(isset($config["model"])?$config["model"]:null);
        });

        Auth::extend('user_guard', function ($app, $name, array $config) {
            $provider = new DefaultUserProvider($config["model"]);
            return new UserGuard($provider);
        });
    }
}
