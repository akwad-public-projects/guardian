<?php

namespace Akwad\Guardian;

use Illuminate\Support\Arr;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Http\Kernel;
use Akwad\Guardian\Http\Middleware\SetLocale;
use Illuminate\Contracts\Foundation\CachesConfiguration;
use Akwad\Guardian\Http\Middleware\DynamicAuthMiddleware;
use Akwad\Guardian\Providers\GuardianAuthenticationServiceProvider;
class GuardianServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/auth.php', 'auth');
        $this->mergeConfigFrom(__DIR__.'/../config/exceptions.php', 'exceptions');
        $this->mergeConfigFrom(__DIR__.'/../config/logging.php', 'logging');
        $this->mergeConfigFrom(__DIR__.'/../config/authentication.php', 'authentication');
        $this->mergeConfigFrom(__DIR__.'/../config/sms.php', 'sms');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Kernel $kernel)
    {
        $this->app->register(GuardianAuthenticationServiceProvider::class);

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'guardian');

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('dynamic.auth', DynamicAuthMiddleware::class); //this middleware is accessed by alias only

        $kernel->pushMiddleware(SetLocale::class); //this middleware is globally applied.
    }

    protected function mergeConfigFrom($path, $key)
    {
        if (! ($this->app instanceof CachesConfiguration && $this->app->configurationIsCached())) {
            $config = $this->app->make('config');
            
            $config->set($key, $this->mergeConfigs(
                require $path, $config->get($key, [])
            ));
        }
    }

    protected function mergeConfigs(array $original, array $merging) {
        $array = $original + $merging;
        foreach ($original as $key => $value) {
           if (! is_array($value)) {
              continue;
           }
           if (! Arr::exists($merging, $key)) {
              continue;
           }
           if (is_numeric($key)) {
              continue;
           }
           $array[$key] = $this->mergeConfigs($value, $merging[$key]);
         }
         return $array;
    }
}
