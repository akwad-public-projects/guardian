<?php

namespace Akwad\Guardian\Exceptions;

use Exception;
use Nuwave\Lighthouse\Exceptions\RendersErrorsExtensions;

class ExceptionHandler extends Exception implements RendersErrorsExtensions
{
    
    protected $code = 0, $message="", $title="", $category = "internal", $isClientSafe = false, $contentParameters = [];

    /**
    * CustomException constructor.
    * 
    * @param  string  $message
    * @param  string  $reason
    * @return void
    */

    public function __construct()
    {
        $key = config("exceptions.{$this->code}");
        $this->message = $this->getContent($key, "message");
        $this->title = $this->getContent($key, "title")." ({$this->code})";
    }

    private function getContent($key, $part){
        return __("exceptions.{$part}.{$key}") != "exceptions.{$part}.{$key}"? __("exceptions.{$part}.{$key}", $this->contentParameters) : __("exceptions.{$part}.default");
    }
    /**
     * Returns true when exception message is safe to be displayed to a client.
     *
     * @api
     * @return bool
     */
    public function isClientSafe(): bool
    {
        return $this->isClientSafe;
    }

    /**
     * Returns string describing a category of the error.
     *
     * Value "graphql" is reserved for errors produced by query parsing or validation, do not use it.
     *
     * @api
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * Return the content that is put in the "extensions" part
     * of the returned error.
     *
     * @return array
     */
    public function extensionsContent(): array
    {
        return [
            'code' => $this->code,
            "title" => $this->title,
        ];
    }
}
