<?php

namespace Akwad\Guardian\Exceptions\SystemExceptions;

use Akwad\Guardian\Exceptions\ExceptionHandler;

class GraphQlSchemaException extends ExceptionHandler
{

    public function __construct($contentParameters = [])
    {
        $this->code = 900;
        $this->category = "internal";
        $this->isClientSafe = true;
        $this->contentParameters = $contentParameters;

        parent::__construct();
    }
}
