<?php

namespace Akwad\Guardian\Exceptions\SystemExceptions;

use Akwad\Guardian\Exceptions\ExceptionHandler;

class GraphQLGeneralException extends ExceptionHandler
{

    public function __construct($contentParameters = [])
    {
        $this->code = 400;
        $this->category = "internal";
        $this->isClientSafe = true;
        $this->contentParameters = $contentParameters;

        parent::__construct();

        if(isset($contentParameters["message"])) $this->message = $contentParameters["message"];
        if(isset($contentParameters["title"])) $this->title = $contentParameters["title"];
    }
}
