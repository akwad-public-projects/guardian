<?php

namespace Akwad\Guardian\Exceptions\SystemExceptions;

use Akwad\Guardian\Exceptions\ExceptionHandler;

class DatabaseGeneralException extends ExceptionHandler
{

    public function __construct($contentParameters = [])
    {
        $this->code = 2000;
        $this->category = "internal";
        $this->isClientSafe = true;
        $this->contentParameters = $contentParameters;

        parent::__construct();
    }
}
