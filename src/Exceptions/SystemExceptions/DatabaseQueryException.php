<?php

namespace Akwad\Guardian\Exceptions\SystemExceptions;

use Akwad\Guardian\Exceptions\ExceptionHandler;

class DatabaseQueryException extends ExceptionHandler
{

    public function __construct($contentParameters = [])
    {
        $this->code = 2100;
        $this->category = "internal";
        $this->isClientSafe = true;
        $this->contentParameters = $contentParameters;

        parent::__construct();
    }
}
