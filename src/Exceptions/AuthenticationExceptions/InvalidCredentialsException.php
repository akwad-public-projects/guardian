<?php

namespace Akwad\Guardian\Exceptions\AuthenticationExceptions;

use Akwad\Guardian\Exceptions\ExceptionHandler;

class InvalidCredentialsException extends ExceptionHandler
{

    public function __construct($contentParameters = [])
    {
        $this->code = 433;
        $this->category = "internal";
        $this->isClientSafe = true;
        $this->contentParameters = $contentParameters;

        parent::__construct();
    }
}
