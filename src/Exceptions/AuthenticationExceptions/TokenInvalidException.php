<?php

namespace Akwad\Guardian\Exceptions\AuthenticationExceptions;

use Akwad\Guardian\Exceptions\ExceptionHandler;

class TokenInvalidException extends ExceptionHandler
{

    public function __construct($contentParameters = [])
    {
        $this->code = 414;
        $this->category = "internal";
        $this->isClientSafe = true;
        $this->contentParameters = $contentParameters;

        parent::__construct();
    }
}
