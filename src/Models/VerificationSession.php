<?php

namespace Akwad\Guardian\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VerificationSession extends Model
{
    use HasFactory;

    protected $fillable = [
        'verified',
        'expires_at',
        'status',
    ];
}
