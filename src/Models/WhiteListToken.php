<?php

namespace Akwad\Guardian\Models;

use Illuminate\Database\Eloquent\Model;

class WhiteListToken extends Model
{
    protected $fillable = [
        'token',
        'auth_user_id',
    ];
}
