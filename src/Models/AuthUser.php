<?php

namespace Akwad\Guardian\Models;

use Illuminate\Foundation\Auth\User;
use Akwad\VoyagerExtension\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;

class AuthUser extends User implements MustVerifyEmail, CanResetPassword
{
    use Notifiable;

    protected $fillable = [
        'type',
        'phone',
        'token',
        'email',
        'password',
        'role',
        'fcm_token',
        'phone_verified_at',
        'last_logged_in',
        'remember_token',
        'locale',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getFirstLoggedAttribute()
    {
        if ($this->last_logged_in != null) {
            return true;
        }
        return false;
    }

    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }

    public function activeImpersonation(){
        return $this->hasMany(Impersonate::class,'impersonator_auth_id','id')->whereNull('cancel_date')->orderBy('created_at','desc');
    }

    public function getRole(){
        return Role::where('name', $this->role)->firstOrFail();
    }
}
