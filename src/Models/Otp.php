<?php

namespace Akwad\Guardian\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable = [
        'code',
        'phone' ,
        'email' ,
        'status' ,
        'message' 
    ];
}
