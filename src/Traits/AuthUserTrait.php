<?php

namespace Akwad\Guardian\Traits;

use Illuminate\Support\Str;
use Akwad\Guardian\Models\AuthUser;
use Illuminate\Support\Facades\Auth;
use Akwad\VoyagerExtension\Models\Role;//Remove From Guardian Package
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\ProfileAlreadyExists;

trait AuthUserTrait{
    protected $authColumns = ['phone','email','token'];

    public function authUser(): BelongsTo
    {
        return $this->belongsTo(AuthUser::class);
    }

    public function setLocaleAttribute($value){
        if(!$value) {
            return;
        }

        $this->attributes['locale'] = $value;

        if(array_key_exists('settings', $this->attributes)){
            $this->settings = $this->settings->merge(['locale' => $value]);
        }
    }

    public function getLocaleAttribute(){
        if(!$this->authUser){
            return "en";
        }
        return $this->authUser->locale;
    }

    public function getPasswordAttribute($value){
        if(!$this->authUser || $value){
            return $value;
        }
        return $this->authUser->password;
    }
    
    protected function createAuthUser(){

        if($this->auth_user_id) return;

        $query = AuthUser::query();
        $modelAttrs = $this->getAttributes();
        $filledField = false;
        foreach ($this->authColumns as $column) {
            $columnValue = isset($modelAttrs[$column]) ? $modelAttrs[$column] : null;

            if(!$columnValue) continue;

            $filledField = true;

            $query->orWhere($column, $columnValue);
        }

        if(!$filledField) return; // return scilently if no filled has values

        $users = $query->get();

         if(count($users) >= 1){
            throw new ProfileAlreadyExists();
        }

        // if(count($users)==1){
        //     $this->auth_user_id = $users[0]->id;
        //     $this->removeAdditionalFields();
        //     return;
        // }

        Auth::guard("AuthUserGuard")->verify($modelAttrs);

        $parts = explode("\\",get_class($this));
        $role = Str::snake(end($parts),'-');

        $authUser = [
            'role' => $role,
            'phone' => isset($modelAttrs['phone']) ? $modelAttrs['phone'] : null,
            'email' => isset($modelAttrs['email']) ? $modelAttrs['email'] : null,
            'token' => isset($modelAttrs['token']) ? $modelAttrs['token'] : null,
            'type' => isset($modelAttrs['social_type']) ? $modelAttrs['social_type'] : null,
            'locale' => isset($modelAttrs['locale']) && $modelAttrs['locale']? $modelAttrs['locale'] : "en",
            'password'=> isset($modelAttrs['password']) ? bcrypt($modelAttrs['password']) : null,
        ];

        $this->removeAdditionalFields();

        $createdAuthUser = AuthUser::create($authUser);

        $this->auth_user_id = $createdAuthUser->id;
    }

    private function removeAdditionalFields(){
        $modelAttrs = $this->getAttributes();
        $this->authColumns[] = 'password';
        $this->authColumns[] = 'locale';
        $this->authColumns[] = 'social_type';

        if (!isset($this->role_id)) {
            foreach ($this->authColumns as $column) {
                if(isset($modelAttrs[$column])){
                    unset($this->{$column});
                }
            }
        }
    }

    protected function updateAuthUser(){

        if(!$this->authUser){
            $this->createAuthUser();
        }

        $this->load('authUser');

        if ($this->isDirty('phone')) {

            $existingAuthUser = AuthUser::wherePhone($this->attributes['phone'])
                ->where('id','!=', $this->authUser->id)->first();

            if ($existingAuthUser) {
                throw new ProfileAlreadyExists();
            }

            $this->authUser->phone = $this->attributes['phone'];
            $this->authUser->save();
        }

        if ($this->isDirty('email')) {

            $existingAuthUser = AuthUser::whereEmail($this->attributes['email'])
                ->where('id','!=', $this->authUser->id)->first();

            if ($existingAuthUser) {
                throw new ProfileAlreadyExists();
            }

            $this->authUser->email = $this->attributes['email'];
            $this->authUser->save();
        }

        if ($this->isDirty('locale') && isset($this->attributes['locale'])) {
            $this->authUser->update([
                'locale' => $this->attributes['locale'],
            ]);
        }

        if ($this->isDirty('password')) {
            $this->authUser->update([
                'password' => $this->password,
            ]);
        }

        $this->authColumns[] = 'password';
        $this->authColumns[] = 'locale';

        if (!isset($this->role_id)) {
            foreach ($this->authColumns as $column) {
                if(isset($this->{$column})){
                    unset($this->{$column});
                }
            }
            return;
        }

        $role = Role::find($this->role_id);

        $data = [
            'email' => $this->email,
            'role' => $role->name
        ];

        if($this->settings->get('locale')) $data['locale'] = $this->settings->get('locale');

        $this->authUser->update($data);
        
    }

    protected static function bootAuthUserTrait()
    {

        static::creating(function ($model) {

            $model->createAuthUser();

        });

        static::updating(function ($model) {

            $model->updateAuthUser();

        });
    }
}
