<?php

namespace Akwad\Guardian\UserProviders\Handlers;

use \DomainException;
use \Firebase\JWT\JWT;
use \UnexpectedValueException;
use \InvalidArgumentException;
use Illuminate\Support\Facades\Log;
use Akwad\Guardian\Models\AuthUser;
use Akwad\Guardian\Models\WhiteListToken;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\TokenInvalidException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\TokenBlacklistedException;

class JwtTokenHandler
{
    public function retrieveByCredentials(array $credentials)
    {
        $token = $credentials['jwt_token'];
        
        $white_listed = WhiteListToken::where('token', $token)->first();
        if(!$white_listed){
            Log::channel('guards')->info("UserGuard: Token is BlackListed");
            throw new TokenBlacklistedException();
        }

        $data = $this->decodeJwtToken($token);
        return AuthUser::find($data->data->id);
    }

    private function decodeJwtToken($token){
        $secretKey = config("authentication.JWT_SECRET");
        $secretKey = base64_decode($secretKey);

        try {

            JWT::$leeway = 60 * 60 * 24; //24 hour
            return JWT::decode($token, $secretKey, ['HS512']);

        } catch (DomainException | InvalidArgumentException | UnexpectedValueException  $e) {
            
            Log::channel('guards')->info("UserGuard: Token is Invalid");
            throw new TokenInvalidException();
        }
    }

    public function verifyCredentials(array $credentials)
    {
        return true;
    }
}