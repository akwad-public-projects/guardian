<?php

namespace Akwad\Guardian\UserProviders\Handlers;

use Carbon\Carbon;
use Akwad\Guardian\Models\Otp;
use Akwad\Guardian\Models\AuthUser;
use Illuminate\Support\Facades\Hash;
use Akwad\Guardian\Models\VerificationSession;
use Illuminate\Contracts\Auth\Authenticatable;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\InvalidOtpException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\NotVerifiedException;

class EmailHandler
{
    public function retrieveByCredentials(array $credentials)
    {
        return AuthUser::where('email', $credentials['email'])->orWhere('phone', $credentials['email'])->first();
    }

    public function validateCredentials(?Authenticatable $user, array $credentials)
    {
        if(isset($credentials['code'])) return $this->validateByOtp($credentials['email'], $credentials['code']);

        if(!$user || !Hash::check($credentials['password'], $user->password)){
            return false;
        }
        return true;
    }

    private function validateByOtp($email, $code){

        $otp = Otp::where("email", $email)->where("code", $code)->where("status", "pending")->where('created_at', '>=', Carbon::now()->subMinutes(config('sms.expire_in_minutes'))->toDateTimeString())->first();
        Otp::where("email", $email)->where("status", "pending")->update(["status" => "expired"]);
        if(!$otp){
            throw new InvalidOtpException();
        }

        return true;
    }

    public function verifyCredentials(array $credentials)
    {
        $verificationSession = VerificationSession::where("verified", $credentials["email"])->where("status", "pending")->where('expires_at', '>', Carbon::now())->first();
        VerificationSession::where("verified", $credentials["email"])->where("status", "pending")->update(["status" => "expired"]);
        if(!$verificationSession){
            throw new NotVerifiedException();
        }

        return true;
    }
}