<?php

namespace Akwad\Guardian\UserProviders\Handlers;

use Akwad\Guardian\Models\AuthUser;
use Illuminate\Contracts\Auth\Authenticatable;

class TokenHandler
{

    public function retrieveByCredentials(array $credentials)
    {
        return AuthUser::where('token',$credentials['token'])->first();
    }

    public function validateCredentials(?Authenticatable $user, array $credentials)
    {
        return true;
    }

    public function verifyCredentials(array $credentials)
    {
        return true;
    }
}
