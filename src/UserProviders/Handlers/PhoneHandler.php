<?php

namespace Akwad\Guardian\UserProviders\Handlers;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Akwad\Guardian\Models\Otp;
use Akwad\Guardian\Models\AuthUser;
use Illuminate\Support\Facades\Hash;
use Akwad\Guardian\Models\VerificationSession;
use Illuminate\Contracts\Auth\Authenticatable;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\InvalidOtpException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\NotVerifiedException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\ProfileNotFoundException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\InvalidCredentialsException;
class PhoneHandler
{
    public function retrieveByCredentials(array $credentials)
    {
        if(!Str::startsWith($credentials['phone'], '+2'))
        {
            $credentials['phone'] = '+2'.$credentials['phone'];
        }

        return AuthUser::whereIn('phone', [$credentials['phone'], substr($credentials['phone'], 2)])->first();
    }

    public function validateCredentials(?Authenticatable $user, array $credentials)
    {
        if($credentials['phone'] && isset($credentials['code'])){
            return $this->validateByOtp($credentials['phone'],$credentials['code']);
        }

        if($credentials['phone'] && isset($credentials['password'])){
            return $this->validateByPassword($credentials['phone'],$credentials['password']);
        }
       
        throw new InvalidCredentialsException();
    }

    private function validateByOtp($phone,$code){

        if(!Str::startsWith($phone, '+2'))
        {
            $phone = '+2'.$phone;
        }

        $otp = Otp::whereIn("phone", [$phone, substr($phone, 2)])->where("code", $code)->where("status", "pending")->where('created_at', '>=', Carbon::now()->subMinutes(config('sms.expire_in_minutes'))->toDateTimeString())->first();
        Otp::whereIn("phone", [$phone,substr($phone, 2)])->where("status", "pending")->update(["status" => "expired"]);
        if(!$otp){
            throw new InvalidOtpException();
        }

        return true;
    }

    private function validateByPassword($phone,$password){
        
        $phone = str_replace(" ","",$phone);

        $checkPhone = $phone;
        if(Str::startsWith($checkPhone, '+2'))
        {
            $checkPhone = substr($checkPhone,2);
        }

        $user = AuthUser::where('phone','LIKE','%'.$checkPhone.'%')->first();

        if(!$user){
            throw new ProfileNotFoundException();
        }

        if(Hash::check($password,$user->password)){
            return true;
        }

        throw new InvalidCredentialsException();
    }

    public function verifyCredentials(array $credentials)
    {
        $verificationSession = VerificationSession::where("verified", $credentials["phone"])->where("status", "pending")->where('expires_at', '>', Carbon::now())->first();
        VerificationSession::where("verified", $credentials["phone"])->where("status", "pending")->update(["status" => "expired"]);
        if(!$verificationSession){
            throw new NotVerifiedException();
        }

        return true;
    }
}
