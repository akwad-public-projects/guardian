<?php

namespace Akwad\Guardian\UserProviders;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as LaravelUserProvider;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\ProfileNotFoundException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\InvalidCredentialsException;

class DefaultUserProvider implements LaravelUserProvider
{
    private $model;

    public function __construct($model = null)
    {
        $this->model = $model;
    }

    public function retrieveById($identifier)
    {
        $model = $this->model?? (class_exists("App\\User")? "App\\User":"App\\Models\\User");
        return $model::findOrFail($identifier);
        //voyager will only login with user model
    }

    public function retrieveByCredentials(array $credentials)
    {
        $handler = $this->getHandler($credentials);
        $authUser = $handler->retrieveByCredentials($credentials);

        return $this->model? $this->retrieveProfile($authUser) : $authUser;
    }

    public function retrieveAuthUserByCredentials(array $credentials)
    {
        $handler = $this->getHandler($credentials);
        $authUser = $handler->retrieveByCredentials($credentials);
        
        return $authUser;
    }

    public function retrieveProfile($authUser)
    {
        $modelUser = $this->model::withoutGlobalScopes()->where('auth_user_id', $authUser->id)->first();

        if(!$modelUser){
            throw new ProfileNotFoundException();
        }

        return $modelUser;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function validateCredentials(?Authenticatable $user, array $credentials)
    {
        $handler = $this->getHandler($credentials);
        return $handler->validateCredentials($user, $credentials);
    }

    public function verifyCredentials(array $credentials)
    {
        $shouldVerifyKeys = config("authentication.SHOULD_VERIFY_KEYS");

        foreach($shouldVerifyKeys as $key)
        {
            if(!isset($credentials[$key])) continue;

            $handler = $this->getHandler([$key => $credentials[$key]]);
            $handler->verifyCredentials([$key => $credentials[$key]]);
        }

        return true;
    }

    public function retrieveByToken($identifier, $token)
    {
        return NULL;
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        return NULL;
    }

    private function getHandler($credentials){

        $handler = $this->getHandlerName($credentials);

        return new $handler;
    }

    private function getHandlerName($credentials){
        $priority = config('authentication.AUTHENTICATION_PRIORITY');
        $keys = array_keys($credentials);

        foreach($priority as $key){
            if(!in_array($key, $keys)) continue;
            $key = str_replace('_', '', ucwords($key, '_'))."Handler";
            if(class_exists("\\Akwad\\Guardian\\UserProviders\\Handlers\\$key")){
                return "\\Akwad\\Guardian\\UserProviders\\Handlers\\$key";
            }
        }

        throw new InvalidCredentialsException();
    }
}
