<?php

namespace Akwad\Guardian\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class SetLocale
{
    public function handle($request, Closure $next)
    {
        $locale = null;

        try {
            $user = Auth::guard('AuthUserGuard')->user();
            $locale = $user->locale;
        } catch (Exception $ex) {}

        $locale = $locale ?? $request->header("Lang") ?? App::getLocale();

        App::setlocale($locale);

        return $next($request);
    }
}
