<?php

namespace Akwad\Guardian\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\ForbiddenException;

class DynamicAuthMiddleware
{
    public function handle($request, Closure $next, $model, $public = null, $token_model = null, $web = null)
    {
        Auth::shouldUse("MyVoyagerGuard"); //Remove From Guardian Package

        if(request()->is('graphql'))
        {
            Auth::shouldUse("{$model}Guard");
        }

        if($web)
        {
            Auth::shouldUse("{$model}WebGuard");
        }

        if(request()->token && $token_model)
        {
            request()->headers->set("Authorization", "Bearer ".$request->token);
            Auth::shouldUse("{$token_model}Guard");
        }

        if(!Auth()->user() && !$public){
            if(request()->is('graphql'))
            {
                throw new ForbiddenException();
            }
            abort(403);
        }

        return $next($request);
    }
}
