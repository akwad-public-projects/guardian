<?php

namespace Akwad\Guardian\Guards;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Akwad\Guardian\Models\WhiteListToken;
use Akwad\Guardian\Helpers\JwtTokenGenerator;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\TokenIsMissingException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\ProfileNotFoundException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\InvalidCredentialsException;
class UserGuard implements Guard
{
    use GuardHelpers;

    protected $request;
    protected $inputKey;
    protected $storageKey;
    protected $hash = false;

    public function __construct($provider)
    {
        $this->provider = $provider;
        
        $this->inputKey = 'api_token';
        $this->storageKey = 'api_token';
        $this->hash = false;
        $this->request = request();
    }

    public function attempt(array $credentials = [])
    {
        $this->user = $this->provider->retrieveByCredentials($credentials);
        if(!$this->user){
            throw new ProfileNotFoundException();
        }

        if(!$this->validate($credentials)) throw new InvalidCredentialsException();

        $this->user->last_logged_in = Carbon::now();
        $this->user->save();
        
        $this->user = JwtTokenGenerator::generateJWTToken($this->user);
        WhiteListToken::updateOrCreate(['auth_user_id' => $this->user->id], ['token' => $this->user->jwtToken]);
        return $this->user;
    }

    public function user()
    {
        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (! is_null($this->user)) {
            return $this->user;
        }

        $this->user = null;

        $token = $this->getTokenForRequest();

        if (! empty($token)) {
            $this->user = $this->provider->retrieveAuthUserByCredentials([
                "jwt_token" => $token,
            ]);
        }

        if($this->provider->getModel() && $this->user) $this->user =  $this->provider->retrieveProfile($this->user);

        return $this->user;
    }

    public function logout(){
        $this->user = $this->user();
        if(!$this->user) throw new TokenIsMissingException();
        
        $this->user->update(['fcm_token' => NULL]);
        WhiteListToken::where('auth_user_id', $this->user->id)->delete();
    }

    public function validate(array $credentials = [])
    {
        return $this->provider->validateCredentials($this->user, $credentials);
    }

    public function verify(array $credentials = [])
    {
        return $this->provider->verifyCredentials($credentials);
    }

    public function getTokenForRequest()
    {
        $token = $this->request->query($this->inputKey);

        if (empty($token)) {
            $token = $this->request->input($this->inputKey);
        }

        if (empty($token)) {
            $token = $this->request->bearerToken();
        }

        if (empty($token)) {
            $token = $this->request->getPassword();
        }

        return $token;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }
}
