<?php

namespace Akwad\Guardian\GraphQL\Mutators;

use Carbon\Carbon;
use \Firebase\JWT\JWT;
use Illuminate\Support\Str;
use Akwad\Guardian\Models\Otp;
use App\Events\PostRegisterEvent;
use Akwad\Guardian\Models\AuthUser;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Akwad\VoyagerExtension\Models\Role;
use GraphQL\Type\Definition\ResolveInfo;
use Akwad\Guardian\Models\WhiteListToken;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\ProfileExistsException;
use App\SMSNotifications\CustomSMSNotification;
use App\Exceptions\PhoneVerificationTimeoutException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Akwad\Guardian\Exceptions\SystemExceptions\GraphQLGeneralException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\ProfileNotFoundException;

class AuthenticationMutator
{
    public function sendOTP($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo){
        
        Otp::where("phone", $args['phone'])->where("status", "pending")->update(["status" => "expired"]);

        $test_numbers = config('sms.test_numbers')??[];
        
        $phone = $args['phone'];
        if(!Str::startsWith($phone, '+2'))
        {
            $phone = '+2'.$phone;
        }

        $code =  mt_rand(100000,999999);

        if(in_array($phone, $test_numbers) || config('sms.test_mode')){
            $code = "123456";
        }
        
        Otp::create([
            'code' => $code,
            'phone' => $args['phone'],
        ]);

        return $code;
    }

    public function login($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $customMessages = [
            'phone.login_auth' => __('exceptions.message.profile_not_found'),
        ];

        $validator = Validator::make($args, [
            'type' => ['string', 'required', 'in:phone,facebook,apple'],
            'phone' => ['required_if:type,phone', 'string'],
            'token' => ['required_if:type,facebook,apple', 'string'],
            'code' => ['string'],//for phone login
            'password' => ['string' , 'min:3'],//for phone login
        ], $customMessages);

        if ($validator->fails()) {

            Log::channel('authentication')->info("RegisterMutator (login): {$validator->messages()->first()}");

            throw new GraphQLGeneralException(["message" => "{$validator->messages()->first()}"]);
        }

        if($args['type'] == 'phone'){

            $validator = Validator::make($args, [
                'phone' => ['login_auth'],
            ], $customMessages);
    
            if ($validator->fails()){

                Log::channel('authentication')->info("RegisterMutator (login): {$validator->messages()->first()}");

                throw new ProfileNotFoundException();
            }
        }

        $user = Auth::guard('AuthUserGuard')->attempt($args);

        Log::channel('authentication')->info("RegisterMutator (login): resolver has finished");
        return $user;
    }

    public function logout($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $authUser = Auth::guard('AuthUserGuard')->user();
        
        $authUser->update(['fcm_token' => NULL]);
        Log::channel('authentication')->info("RegisterMutator (logout): Auth User {$authUser->id} cleared FCM token");

        WhiteListToken::where('auth_user_id', $authUser->id)->delete();
        Log::channel('authentication')->info("RegisterMutator (logout): deleted whitelist Token");

        $authUser->activeImpersonation()->update([
            'cancel_date' => Carbon::now()
        ]);
        
        Log::channel('authentication')->info("RegisterMutator (logout): resolver has finished");
        return "User logged out successfully";
    }

    public function refreshToken($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $authUser = Auth::guard('AuthUserGuard')->user();

        $secretKey = config("authentication.JWT_SECRET");
        $secretKey = base64_decode($secretKey);
        $userTokenHeader = $context->request->header('Authorization');
        list($token) = sscanf($userTokenHeader, 'Bearer %s');

        try {
            JWT::$leeway = 60 * 60 * 24; //24 hour
            $decoded = (array) JWT::decode($token, $secretKey, ['HS512']);
            $decoded['iat'] = time();

            $expire = time() + (30 * 86400); // Adding 86400 seconds (1 Month)
            $decoded['exp'] = $expire;

            $generatedToken = JWT::encode($decoded, $secretKey, 'HS512');
            $whiteListToken = WhiteListToken::updateOrCreate(['auth_user_id' => $authUser->id], ['token' => $generatedToken]);

            Log::channel('authentication')->info("RegisterMutator (refreshToken): resolver has finished");
            $authUser->jwtToken = $generatedToken;
            $authUser->expire = new Carbon($expire);
            return $authUser;
        } catch (\Throwable $th) {
            Log::channel('authentication')->warning("RegisterMutator (refreshToken): {$th}");
            throw new GraphQLGeneralException(["message" => $th]);
        }
    }

    public function update($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $validator = Validator::make($args, [
            'fcm_token' => ['string', 'required'],
        ]);

        if ($validator->fails()) {
            Log::channel('authentication')->info("RegisterMutator (update): {$validator->messages()->first()}");
            throw new GraphQLGeneralException(["message" => "{$validator->messages()->first()}"]);
        }
        
        $authUser = Auth::guard('AuthUserGuard')->user();
        if ($authUser) {
            $authUser->update(['fcm_token' => $args['fcm_token']]);
            Log::channel('authentication')->info("RegisterMutator (update): Auth User {$authUser->id} updated FCM token");
        }
        Log::channel('authentication')->info("RegisterMutator (update): resolver has finished");
        return 'FCM Token updated successfully';
    }

    protected function generateJWTToken($user)
    {

        $secretKey = config("authentication.JWT_SECRET");

        // $tokenId = base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt + 10; //Adding 10 seconds
        $expire = $notBefore + (30 * 86400); // Adding 86400 seconds (1 Month)
        $serverName = "Sercl server"; // Retrieve the server name from config file

        /*
         * Create the token as an array
         */
        $data = [
            'iat' => $issuedAt, // Issued at: time when the token was generated
            // 'jti' => $tokenId, // Json Token Id: an unique identifier for the token
            'iss' => $serverName, // Issuer
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => $user, // Data related to the signer user
        ];

        /*
         * Extract the key, which is coming from the config file.
         *
         * Best suggestion is the key to be a binary string and
         * store it in encoded in a config file.
         *
         * Can be generated with base64_encode(openssl_random_pseudo_bytes(64));
         *
         * keep it secure! You'll need the exact key to verify the
         * token later.
         */

        $secretKey = base64_decode($secretKey);

        /*
         * Encode the array to a JWT string.
         * Second parameter is the key to encode the token.
         *
         * The output string can be validated at http://jwt.io/
         */
        $jwt = JWT::encode(
            $data, //Data to be encoded in the JWT
            $secretKey, // The signing key
            'HS512' // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
        );
        $user->jwtToken = $jwt;
        $user->expire = new Carbon($expire);
        return $user;
    }

    public function verifyUserPhone($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo){

        $signup = isset($args['sign_up'])? $args['sign_up'] : false;

        $checkPhone = $args['phone'];
        if(Str::startsWith($checkPhone, '+2'))
        {
            $checkPhone = substr($checkPhone,2);
        }

        $user = AuthUser::where('phone','LIKE','%'.$checkPhone.'%')->first();

        if(!$user && ( (isset($args['role']) && $args['role'] != 'customer') || !isset($args['role']) ) ){
            return 'Phone number verified';
        }

        Auth::guard("AuthUserGuard")->validate($args);
        if($signup && $user && $user->password){
            $model = Role::where('name', $args['role'])->first()->model;
            $profile = $model::where('auth_user_id',$user->id)->first();
            if($profile) throw new ProfileExistsException();
        }

        if(!$user){

            $user = AuthUser::create([
                'token' => isset($args["token"])? $args["token"] : null,
                'phone' => isset($args["phone"])? $args["phone"] : null,
                'type' => isset($args['type'])? $args['type'] : 'phone',
                'role' => $args['role'],
            ]);
        }

        $user->update([
            'phone_verified_at' => Carbon::now()
        ]);

        if(isset($args['role']) && $args['role'] == 'customer'){
            event(new PostRegisterEvent($user));
        }

        return 'Phone number verified';
    }

    public function resetPassword($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo){
      
        $validator = Validator::make($args, [
            'password' => ['string', 'required', 'min:3','confirmed'],
        ]);

        if ($validator->fails()) {
            Log::channel('authentication')->info("resetPassword: {$validator->messages()->first()}");
            throw new GraphQLGeneralException(["message" => "{$validator->messages()->first()}", "title" => __("exceptions.title.reset_password_mismatch")]);
        }

        $checkPhone = $args['phone'];
        if(Str::startsWith($checkPhone, '+2'))
        {
            $checkPhone = substr($checkPhone,2);
        }

        $authUser = AuthUser::where('phone','LIKE','%'.$checkPhone.'%')->first();
        
        if(!$authUser){
            throw new ProfileNotFoundException();
        }
        
        if($authUser->phone_verified_at < Carbon::now()->subMinutes(3)){
            throw new PhoneVerificationTimeoutException();
        }

        $authUser->update([
            'password' =>  Hash::make($args['password']),
        ]);
        
        return __('auth.password_reset');
    }
}
